package ru.levelup.banks.library.repository;

import ru.levelup.banks.library.model.Bank;
import ru.levelup.banks.library.model.Person;

import java.util.List;

public interface BankRepository {

    Bank createBank(String name);

    void addClient(Integer bankId, Person person);

    List<Bank> findAllBanks();

}
