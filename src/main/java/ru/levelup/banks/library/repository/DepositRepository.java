package ru.levelup.banks.library.repository;

import ru.levelup.banks.library.model.Deposit;

public interface DepositRepository {

    Deposit createDeposit(String name, double rate, boolean canPartialRemoval, boolean canReplenishment, double minAmount);

}
