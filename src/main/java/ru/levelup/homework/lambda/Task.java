package ru.levelup.homework.lambda;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@EqualsAndHashCode
@ToString
@Getter
@Setter
public class Task {
    private final String id;
    private final String title;
    private final TaskType type;
    private final LocalDate createdOn;
    private boolean done = false;
    private Set<String> tags = new HashSet<>();
    private LocalDate dueOn;

    public Task(String id, String title, TaskType type, LocalDate createdOn, LocalDate dueOn) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.createdOn = createdOn;
        this.dueOn = dueOn;
    }

    public void addTag(String string) {
        tags.add(string);
    }


    public static void main(String[] args) {

        List<Task> tasks = createTasksList();

//        System.out.println(tasks);


//  !      1. Найти все задачи типа READING, отсортированные по дате создания.
        List<Task> sortedTasks_1 = tasks.stream().filter(u1 -> u1.getType() == TaskType.Reading)
//                .sorted((o1, o2) -> o1.getDueOn().isBefore(o2.getDueOn()))
                .collect(Collectors.toList());
        System.out.println("1. Найти все задачи типа READING, отсортированные по дате создания:  \n" + sortedTasks_1);


//     ?   2. Получить список задач, которые уникальны (уникальность проверяйте по полям title и type - и сделайте
//        это с помощью аннотаций Lombok - самим метод equals и hashCode переопределять не нужно).
        List<Task> sortedTasks_2 = tasks.stream().distinct().collect(Collectors.toList());

        System.out.println("2. Получить список задач, которые уникальны :  \n" + sortedTasks_2);


//  !!!!     3. Найти пять первых задач типа READING, отсортированных по дате создания
        List<Task> sortedTasks_3 = tasks.stream().filter(u1 -> u1.getType() == TaskType.Reading)
                .limit(5)
                .collect(Collectors.toList());

        System.out.println("3. Найти пять первых задач типа READING, отсортированных по дате создания: \n" + sortedTasks_3);

//   !!!!     4. Посчитать количество задач, у которых type - CODING.
        int sortedTasks_4 = Math.toIntExact(tasks.stream().filter(u1 -> u1.getType() == TaskType.Coding).count());

        System.out.println("4. Посчитать количество задач, у которых type - CODING: " + sortedTasks_4);

//   !!!    5. Проверить, у всех ли задач, у которых стоит type READING, есть тег #books

        int sortedTasks_5_1 = Math.toIntExact(tasks.stream().filter(u1 -> u1.getType() == TaskType.Reading)
                .filter(u1 -> !u1.getTags().toString().contains("books"))
                .count());

        int sortedTasks_5_2 = Math.toIntExact(tasks.stream().filter(u1 -> u1.getType() == TaskType.Reading)
                .filter(u1 -> u1.getTags().toString().contains("books"))
                .count());

        System.out.println("5. Количество задач READING без тегом #books: " + sortedTasks_5_1);
        System.out.println("5. Количество задач READING с тегом #books: " + sortedTasks_5_2);

//  !!!      6. Собрать все заголовки задач в единую строку (используйте разделитель ***)

        List<String> sortedTasks_6 = tasks.stream().map(task -> task.getTitle()).collect(Collectors.toList());
        String result_task_6 = "";

        for (String title : sortedTasks_6) {
            result_task_6+=title +"***";
        }
        result_task_6=result_task_6.substring(0,result_task_6.length()-3);

        System.out.println(" 6. Собрать все заголовки задач в единую строку (используйте разделитель ***): \n " + result_task_6);
    }


    public static List<Task> createTasksList() {
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(new Task("1",
                "Write this 1",
                TaskType.Writing,
                LocalDate.of(2020, 1, 8),
                LocalDate.of(2020, 12, 18)));


        tasks.add(new Task("2",
                "Code this 2",
                TaskType.Coding,
                LocalDate.of(2020, 11, 6),
                LocalDate.of(2020, 12, 28)));

        tasks.add(new Task("3",
                "Read this 3",
                TaskType.Reading,
                LocalDate.of(2020, 10, 8),
                LocalDate.of(2020, 12, 18)));
        tasks.get(2).addTag("books");

        tasks.add(new Task("4",
                "Write that 4",
                TaskType.Writing,
                LocalDate.of(2020, 9, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("5",
                "Code that 5",
                TaskType.Coding,
                LocalDate.of(2020, 1, 6),
                LocalDate.of(2021, 1, 8)));

        tasks.add(new Task("6",
                "Read that 6",
                TaskType.Reading,
                LocalDate.of(2020, 11, 6),
                LocalDate.of(2021, 03, 5)));
        tasks.get(5).addTag("books");

        tasks.add(new Task("7",
                "Write this 7",
                TaskType.Writing,
                LocalDate.of(2020, 1, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("8",
                "Code this 8",
                TaskType.Coding,
                LocalDate.of(2020, 11, 6),
                LocalDate.of(2020, 12, 28)));

        tasks.add(new Task("9",
                "Read this 9",
                TaskType.Reading,
                LocalDate.of(2020, 10, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("10",
                "Write that 10",
                TaskType.Writing,
                LocalDate.of(2020, 9, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("11",
                "Code that 11",
                TaskType.Coding,
                LocalDate.of(2020, 1, 6),
                LocalDate.of(2021, 1, 8)));

        tasks.add(new Task("12",
                "Read that 12",
                TaskType.Reading,
                LocalDate.of(2020, 11, 6),
                LocalDate.of(2021, 03, 5)));
        tasks.add(new Task("13",
                "Write this 13",
                TaskType.Writing,
                LocalDate.of(2020, 1, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("15",
                "Code this 15",
                TaskType.Coding,
                LocalDate.of(2020, 11, 6),
                LocalDate.of(2020, 12, 28)));

        tasks.add(new Task("9",
                "Read this 9",
                TaskType.Reading,
                LocalDate.of(2020, 10, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("16",
                "Write that 16",
                TaskType.Writing,
                LocalDate.of(2020, 9, 8),
                LocalDate.of(2020, 12, 18)));

        tasks.add(new Task("17",
                "Code that 17",
                TaskType.Coding,
                LocalDate.of(2020, 1, 6),
                LocalDate.of(2021, 1, 8)));

        tasks.add(new Task("18",
                "Read that 18",
                TaskType.Reading,
                LocalDate.of(2020, 11, 6),
                LocalDate.of(2021, 03, 5)));

        return tasks;
    }


}
