package ru.levelup.lessons.reflection.annotation.homework5;


import lombok.ToString;
import ru.levelup.lessons.reflection.annotation.ReflectionClass;

@ReflectionClass
@ToString
public class SampleClass_1 {
    int number;
    String text;
    String text_new;

    public SampleClass_1() {
        number = 100;
        text = "Hello";
        text_new = "Hello again";
    }
}
