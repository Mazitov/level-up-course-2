package ru.levelup.lessons.reflection.annotation.homework5;


import lombok.ToString;
import ru.levelup.lessons.reflection.annotation.ReflectionClass;

@ReflectionClass
@ToString
public class SampleClass_2 {

    String how;
    String where;
    String why;

    public SampleClass_2() {
        how = "easy";
        where = where;
        why = why;
    }
}
