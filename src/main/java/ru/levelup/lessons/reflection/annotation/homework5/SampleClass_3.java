package ru.levelup.lessons.reflection.annotation.homework5;

import lombok.ToString;
import ru.levelup.lessons.reflection.annotation.ReflectionClass;

@ReflectionClass
@ToString
public class SampleClass_3 {
    String version;
    Double percent;
    Long big_number;

    public SampleClass_3() {
        version = "New";
        percent = 55.5;
        big_number = 457897978L;
    }
}
