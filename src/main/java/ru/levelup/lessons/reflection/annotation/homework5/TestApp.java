package ru.levelup.lessons.reflection.annotation.homework5;

import javassist.tools.reflect.Reflection;
import lombok.SneakyThrows;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class TestApp {


    private static Object ReflectionClass;

    @SneakyThrows
    public static void main(String[] args) {


        List<Class<?>> foundClasses =
                findClasses(new File(
                                "C:\\Users\\mdmit\\Downloads\\Java_2_course_3_2020\\src\\main\\java\\ru\\levelup\\lessons"),
                        "ru.levelup.lessons");

        System.out.println(foundClasses.toString());
        for (Class class_1 : foundClasses) {
            Annotation[] annotations = class_1.getAnnotations();
            for (Annotation ann : annotations) {
                if (ann.annotationType().getSimpleName().equals("ReflectionClass")) {
                    System.out.println(class_1.newInstance().toString());
                }
            }
        }
    }

    @SneakyThrows
    private static List<Class<?>> findClasses(File directory, String packageName) {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
                System.out.println(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".java")) {
                classes.add(Class.forName(packageName + '.'
                        + file.getName().substring(0, file.getName().length() - 5)));
                System.out.println(Class.forName(packageName + '.'
                        + file.getName().substring(0, file.getName().length() - 5)));
            }
        }
        return classes;
    }

}
