package ru.levelup.lessons.reflection.proxy;

import lombok.RequiredArgsConstructor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

@RequiredArgsConstructor
public class LogTimeExecutionAnnotationInvocationHandler implements InvocationHandler {

    private final DefaultLoginService loginService;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LogTimeExecution annotation = loginService.getClass()
                .getDeclaredMethod(method.getName(), method.getParameterTypes())
                .getAnnotation(LogTimeExecution.class);

        if (annotation != null) {
            long start = System.nanoTime();

            Object result = method.invoke(loginService, args);

            long end = System.nanoTime();
            System.out.println("Execution time: " + (end - start));

            return result;
        }

        return method.invoke(loginService, args);
    }

}
