package ru.levelup.lessons.reflection.proxy;

public interface LoginService {

    void login(String login, String password);

    void logout(String login);

}
