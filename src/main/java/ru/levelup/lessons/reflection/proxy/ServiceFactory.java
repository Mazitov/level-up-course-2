package ru.levelup.lessons.reflection.proxy;

import java.lang.reflect.Proxy;

public class ServiceFactory {
    
    public LoginService createLoginService() {
        DefaultLoginService loginService = new DefaultLoginService();
        return proxyServiceObject(loginService);
    }
    
    private LoginService proxyServiceObject(DefaultLoginService service) {
        LoginService proxyInstance = (LoginService) Proxy.newProxyInstance(
                service.getClass().getClassLoader(),
                service.getClass().getInterfaces(),
                new LogTimeExecutionAnnotationInvocationHandler(service)
        );

        return proxyInstance;
    }
    
}
