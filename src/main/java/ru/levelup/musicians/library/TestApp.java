package ru.levelup.musicians.library;

import org.hibernate.SessionFactory;
import ru.levelup.musicians.library.hibernate.HibernateUtils;
import ru.levelup.musicians.library.model.Band;
import ru.levelup.musicians.library.model.Country;
import ru.levelup.musicians.library.model.Musician;
import ru.levelup.musicians.library.repository.BandRepository;
import ru.levelup.musicians.library.repository.CountryRepository;
import ru.levelup.musicians.library.repository.MusicianRepository;
import ru.levelup.musicians.library.repository.impl.HibernateBandRepository;
import ru.levelup.musicians.library.repository.impl.HibernateCountryRepository;
import ru.levelup.musicians.library.repository.impl.HibernateMusicRepository;


public class TestApp {

    public static void main(String[] args) {
        SessionFactory factory = HibernateUtils.getFactory();
        BandRepository bandRepository = new HibernateBandRepository(factory);
        Band bandbyID = bandRepository.findByID(Integer.parseInt("1"));
        System.out.println(bandbyID);



        CountryRepository countryRepository = new HibernateCountryRepository(factory);
        Country countryById = countryRepository.findById(Integer.parseInt("1"));
        System.out.println(countryById.toString());
        
        MusicianRepository musicianRepository = new HibernateMusicRepository(factory);

        Musician musicianById = musicianRepository.findById(Integer.parseInt("1"));
        System.out.println(musicianById.toString());

      
////
//        Collection<Country> byName = countryRepository.findAllCountries();
//        System.out.println(byName.toString());


        factory.close();
    }
}
