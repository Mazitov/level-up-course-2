package ru.levelup.musicians.library.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public final class HibernateUtils {

    private HibernateUtils() {
    }

    private static SessionFactory factory;

    static {
        Configuration configuration = new Configuration().configure("hibernate.cfg.music.xml");
        factory = configuration.buildSessionFactory();

    }

    public static SessionFactory getFactory() {
        return factory;
    }


}
