//package ru.levelup.musicians.library.jdbc;
//
//import ru.levelup.musicians.library.jdbc.mapper.CountryMapper;
//import ru.levelup.musicians.library.model.Country;
//
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.Collection;
//
//public class CountriesJdbcService {
//
//    private JdbcConnectionService jdbcConnectionService;
//    private CountryMapper countryMapper;
//
//    public CountriesJdbcService() {
//        this.jdbcConnectionService = new JdbcConnectionService();
//        this.countryMapper = new CountryMapper();
//    }
//
//
//    public Collection<Country> findCountries() {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            Statement stmt = connection.createStatement(); // оболочка над sql запросом
//            ResultSet rs = stmt.executeQuery("select * from countries");
//            Collection<Country> countries = new ArrayList<>();
//            while (rs.next()) {
//                countries.add(countryMapper.mapResultSet(rs));
//            }
//            System.out.println(countries);
//            return countries;
//
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not execute findCountries method", exc);
//        }
//
//    }
//
//    public Country findCountry(int country_id) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            Statement stmt = connection.createStatement(); // оболочка над sql запросом
//            String sql = "select * from countries where country_id = \'" + country_id + "\'";
//            ResultSet rs = stmt.executeQuery(sql);
//            int id = 0;
//            String name = null;
//            int id_num = 0;
//            while (rs.next()) {
//                id = rs.getInt(1);
//                name = rs.getString(2);
//                id_num++;
//            }
//            if (id_num > 1)
//                throw new RuntimeException("There are " + id_num + " countries with the same ID - " + country_id + ".");
//            return Country.builder().country_id(id).country_name(name).build();
//
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not execute findCountries method", exc);
//        }
//
//    }
//
//    public Country findCountry(String country_name) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            Statement stmt = connection.createStatement(); // оболочка над sql запросом
//            String sql = "select * from countries where lower(country_name) = \'" + country_name.toLowerCase() + "\'";
//            ResultSet rs = stmt.executeQuery(sql);
//            int id = 0;
//            String name = null;
//            int id_num = 0;
//            while (rs.next()) {
//                id = rs.getInt(1);
//                name = rs.getString(2);
//                id_num++;
//            }
//            if (id_num > 1)
//                throw new RuntimeException("There are " + id_num + " countries with the same name - " + country_name + ".");
//            if (id_num == 0)
//                throw new RuntimeException("There no country with name with name - " + country_name + ".");
//            return Country.builder().country_id(id).country_name(name).build();
//
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not execute findCountry method", exc);
//        }
//    }
//
//    public Country createCountry(String country_name) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            String sql = "insert into countries values (nextval('country_id_sequence'), ?)";
//            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            stmt.setString(1, country_name);
//            int affectedRows = stmt.executeUpdate();
//            System.out.println("Affected rows: " + affectedRows);
//            ResultSet keysRS = stmt.getGeneratedKeys();
//            keysRS.next();
//            int generateKey = keysRS.getInt(1);
//            return Country.builder()
//                    .country_id(generateKey)
//                    .country_name(country_name)
//                    .build();
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not create new country", exc);
//        }
//    }
//
//    public boolean deleteCountry(int countryId) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            String sql = "delete from countries where country_id = ?";
//            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            stmt.setInt(1, countryId);
//            int affectedRows = stmt.executeUpdate();
//            System.out.println("Affected rows by deletion of country with id " + countryId + ": " + affectedRows);
//            if (affectedRows == 0) throw new SQLException("Could not delete country with ID " + countryId);
//            return true;
//        } catch (SQLException exc) {
//            System.out.println(exc);
//            return false;
//        }
//    }
//}
