//package ru.levelup.musicians.library.jdbc;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//public class JdbcConnectionService {
//
//    static {
//        registerJdbcDriver();
//    }
//
//    private static void registerJdbcDriver(){
//        try {
//            Class.forName("org.postgresql.Driver"); // загразка класса в память (всё что static)
//        } catch (ClassNotFoundException e){
//            throw new RuntimeException("Could not register JDBC driver", e);
//        }
//    }
//
//    public Connection openConnectionBanksDB() {
//        // объектое представление физического соединения к базе
//        try {
//            return DriverManager.getConnection(
//                     //jdbc:<vendor_name>://<host>:<port>/<db_name>?<external_parameter>
//                     "jdbc:postgresql://127.0.0.1:5432/banks",
//                     "postgres",
//                     "admin"
//             );
//        } catch (SQLException exc) {
//           throw new RuntimeException("Could not open connection to database", exc);
//        }
//    }
//
//    public Connection openConnection() {
//        // объектое представление физического соединения к базе
//        try {
//            return DriverManager.getConnection(
//                    //jdbc:<vendor_name>://<host>:<port>/<db_name>?<external_parameter>
//                    "jdbc:postgresql://127.0.0.1:5432/Music",
//                    "postgres",
//                    "admin"
//            );
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not open connection to database", exc);
//        }
//    }
//
//}
