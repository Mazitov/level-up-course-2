//package ru.levelup.musicians.library.jdbc;
//
//import ru.levelup.musicians.library.jdbc.mapper.MusicianMapper;
//import ru.levelup.musicians.library.model.Musician;
//
//import java.sql.*;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Date;
//
//public class MusiciansJdbcService {
//
//
//    private JdbcConnectionService jdbcConnectionService;
//    private MusicianMapper musicianMapper;
//
//    public MusiciansJdbcService() {
//        this.jdbcConnectionService = new JdbcConnectionService();
//        this.musicianMapper = new MusicianMapper();
//    }
//
//    public Collection<Musician> findByName(String name) {
//        try {
//            Connection connection = jdbcConnectionService.openConnection();
//            Statement stmt = connection.createStatement();
//            String sql = ("select * from musicians where first_name = \'" + name + "\';");
//            System.out.println(sql);
//            ResultSet rs = stmt.executeQuery(sql);
//            Collection<Musician> musicians = new ArrayList<>();
//            while (rs.next()) {
//                musicians.add(musicianMapper.mapResultSet(rs));
//            }
//            return musicians;
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not find musician with name - " + name, exc);
//        }
//    }
//
//    public Collection<Musician> findByDateOfBirth(Date dateOfBirth) {
//        try {
//            Connection connection = jdbcConnectionService.openConnection();
//            Statement stmt = connection.createStatement();
//            String date_format = "yyyy-MM-dd";
//            DateFormat formatter = new SimpleDateFormat(date_format);
//            String date = formatter.format(dateOfBirth);
//            String sql = ("select * from musicians where date_of_birth = \'" + date + "\';");
//            System.out.println(sql);
//            ResultSet rs = stmt.executeQuery(sql);
//            Collection<Musician> musicians = new ArrayList<>();
//            while (rs.next()) {
//                musicians.add(musicianMapper.mapResultSet(rs));
//            }
//            return musicians;
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not find musician with date of birth - " + dateOfBirth, exc);
//        }
//    }
//
//    public Musician createMusician(String firstName, String middleName, String lastName, String sex, Date dateOfBirth, int countryId) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            String sql = "insert into musicians values (nextval('musician_id_sequence'), ?, ?, ?, ?, ?, ?);";
//            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//
//            stmt.setString(1, firstName);
//            stmt.setString(2, middleName);
//            stmt.setString(3, lastName);
//            stmt.setString(4, sex);
//            stmt.setDate(5, new java.sql.Date(dateOfBirth.getTime()));
//            stmt.setInt(6, countryId);
//            System.out.println(stmt);
//
//            int affectedRows = stmt.executeUpdate();
//            System.out.println("Affected rows: " + affectedRows);
//            ResultSet keysRS = stmt.getGeneratedKeys();
//            keysRS.next();
//            int generateKey = keysRS.getInt(1);
//            return Musician.builder()
//                    .id(generateKey)
//                    .first_name(firstName)
//                    .middle_name(middleName)
//                    .last_name(lastName)
//                    .sex(sex)
//                    .date_of_birth((java.sql.Date) dateOfBirth)
//                    .country_id(countryId)
//                    .build();
//
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not create new country", exc);
//        }
//
//    }
//
//
//    public boolean deleteMusician(int id) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            String sql = "delete from musicians where id = ?";
//            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            stmt.setInt(1, id);
//            int affectedRows = stmt.executeUpdate();
//            System.out.println("Affected rows by deletion of musician with id " + id + ": " + affectedRows);
//            if (affectedRows == 0) throw new SQLException("Could not delete musician with ID " + id);
//            return true;
//        } catch (SQLException exc) {
//            System.out.println(exc);
//            return false;
//        }
//
//
//    }
//
//
//    public boolean deleteMusician(String firstName, String lastName, Date dateOfBirth) {
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            String sql = "delete from musicians where fist_name = ? and last_name = ? and date_of_birth = ?";
//            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            stmt.setString(1, firstName);
//            stmt.setString(1, lastName);
//            stmt.setDate(1, new java.sql.Date(dateOfBirth.getTime()));
//            int affectedRows = stmt.executeUpdate();
//            System.out.println("Affected rows by deletion of musician with fist name " + firstName + " last name "
//                    + lastName + " and date of birth " + dateOfBirth + " : " + affectedRows);
//            if (affectedRows == 0)
//                throw new SQLException("Could not delete musician with fist name " + firstName + " last name "
//                        + lastName + " and date of birth " + dateOfBirth);
//            return true;
//        } catch (SQLException exc) {
//            System.out.println(exc);
//            return false;
//        }
//
//
//    }
//
//
//    public boolean updateMusician(int id, int country_id) { //Change Country of Musician
//        try (Connection connection = jdbcConnectionService.openConnection()) {
//            String sql = "update musicians set country_id =?  where id =?";
//            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
//            stmt.setInt(1, country_id);
//            stmt.setInt(2, id);
//            int affectedRows = stmt.executeUpdate();
//            System.out.println("Affected rows by update : " + affectedRows);
//            if (affectedRows == 0)
//                throw new SQLException("Could not update musician with id" + id + " and country_id " + country_id);
//            return true;
//        } catch (SQLException exc) {
//            throw new RuntimeException("Could not update musician with id" + id + " and country_id " + country_id, exc);
//        }
//    }
//
//
//}
