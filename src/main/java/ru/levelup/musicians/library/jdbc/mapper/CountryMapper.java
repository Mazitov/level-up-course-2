//package ru.levelup.musicians.library.jdbc.mapper;
//
//import ru.levelup.musicians.library.model.Country;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//public class CountryMapper {
//
//    public Country mapResultSet(ResultSet rs) throws SQLException {
//        return Country.builder()
//                .country_id(rs.getInt("country_id"))
//                .country_name(rs.getString("country_name"))
//                .build();
//    }
//
//}
