package ru.levelup.musicians.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "albums")
@Getter
@Setter
public class Album {

    @Id
    private Integer album_id;
    private String album_name;
    private Date album_year;


}
