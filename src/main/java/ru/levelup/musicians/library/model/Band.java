package ru.levelup.musicians.library.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name = "bands")
@ToString
@NoArgsConstructor
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="band_id", nullable = false)
    private Integer band_id;

    @Column(nullable = false)
    private String band_name;

    @Column(nullable = false)
    private String years_active;

    @OneToOne
    @JoinColumn(name="country_id")
    private Country country;


}
