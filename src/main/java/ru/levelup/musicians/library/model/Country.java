package ru.levelup.musicians.library.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@Entity
@ToString(exclude = "musicians")
@Table(name = "countries")
@NoArgsConstructor
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id", updatable = false, nullable = false)
    private Integer countryId;

    @Column(name = "country_name", nullable = false)
    private String country_name;

    @OneToMany (mappedBy = "country", fetch = FetchType.EAGER)
    private List<Musician> musicians;


    public Country(Integer countryId) {
        this.countryId = countryId;
    }

    public Country(Integer countryId, String country_name) {
        this.countryId = countryId;
        this.country_name = country_name;
    }
}
