package ru.levelup.musicians.library.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@Table(name = "genres")
public class Genre {

    @Id
    private Integer genre_id;

    private String genre_name;


}
