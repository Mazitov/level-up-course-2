package ru.levelup.musicians.library.model;

import lombok.*;
import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.sql.Date;


@Getter
@Setter
@Entity
@Builder
@Table(name = "musicians")
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Musician {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable = false, updatable = false)
    private Integer id;
    private String first_name;
    private String middle_name;
    private String last_name;
    private String sex;

    @Column
    @Type(type = "date")
    private Date date_of_birth;

    @ManyToOne(optional = false)
    @JoinColumn(name = "country_id")
    private Country country;



}
