package ru.levelup.musicians.library.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Date;


@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@Table(name = "musicians_bands")
public class Musician_in_Band {

    @Id
    private Integer musician_id;

    private Integer band_id;

    private Date start_date;
    private Date end_date;




}
