package ru.levelup.musicians.library.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@Table(name = "songs")
public class Song {

    @Id
    private  Integer song_id;

    @ManyToOne
    private  Album album;

    private String song_name;



}
