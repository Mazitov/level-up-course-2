package ru.levelup.musicians.library.repository;

import ru.levelup.musicians.library.model.Band;

import javax.persistence.criteria.CriteriaBuilder;

public interface BandRepository {


    Band findByID(Integer id);
}
