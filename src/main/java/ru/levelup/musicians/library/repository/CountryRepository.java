package ru.levelup.musicians.library.repository;

import ru.levelup.musicians.library.model.Country;

import java.util.Collection;

public interface CountryRepository {

    Country createNewCountry(Integer country_id, String country_name);

    Country findById(Integer country_id);

     Collection<Country> findAllCountries();

}
