package ru.levelup.musicians.library.repository;

import ru.levelup.musicians.library.model.Musician;

public interface MusicianRepository {

//    Musician createMusician(String firstName, String middleName, String lastName, String sex, Date dateOfBirth, int countryId);
//
//    boolean deleteMusician(int id);
//
//    boolean deleteMusician(String firstName, String lastName, Date dateOfBirth);
//
//    boolean updateMusician(int id, int country_id);
//
//    Musician findByName();
//
    Musician findById(Integer id);
//
//    List<Musician> findByName(String name);
//
//    List<Musician> findByDateOfBirth(Date dateOfBirth);


}
