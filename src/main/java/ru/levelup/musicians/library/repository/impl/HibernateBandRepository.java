package ru.levelup.musicians.library.repository.impl;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.levelup.musicians.library.model.Band;
import ru.levelup.musicians.library.model.Country;
import ru.levelup.musicians.library.repository.BandRepository;

@RequiredArgsConstructor
public class HibernateBandRepository implements BandRepository{
        private final SessionFactory factory;



        @Override
    public Band findByID(Integer id) {
            try (Session session = factory.openSession()) {
                return session.get(Band.class, id);
            }
    }
}
