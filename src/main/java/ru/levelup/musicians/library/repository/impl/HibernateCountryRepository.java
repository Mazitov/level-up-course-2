package ru.levelup.musicians.library.repository.impl;


import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.levelup.musicians.library.model.Country;
import ru.levelup.musicians.library.repository.CountryRepository;

import javax.persistence.Query;
import java.util.Collection;



@RequiredArgsConstructor
public class HibernateCountryRepository implements CountryRepository {
    private final SessionFactory factory;

    @Override
    public Country createNewCountry(Integer country_id, String country_name) {
        return null;
    }

    @Override
    public Country findById(Integer country_id) {
        try (Session session = factory.openSession()) {
            return session.get(Country.class, country_id);
        }
    }

    @Override
    public Collection<Country> findAllCountries() {
        try (Session session = factory.openSession()) {
            Query query = session.createQuery("from countries", Country.class);
            Collection<Country> countries = query.getResultList();
            return countries;
        }
    }
}
