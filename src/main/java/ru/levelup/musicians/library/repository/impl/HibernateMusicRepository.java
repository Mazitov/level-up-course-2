package ru.levelup.musicians.library.repository.impl;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.levelup.musicians.library.model.Country;
import ru.levelup.musicians.library.model.Musician;
import ru.levelup.musicians.library.repository.MusicianRepository;

@RequiredArgsConstructor
public class HibernateMusicRepository implements MusicianRepository {
    private final SessionFactory factory;

    @Override
    public Musician findById(Integer id) {

        try (Session session = factory.openSession()) {
            return session.get(Musician.class, id);
        }
    }


}
